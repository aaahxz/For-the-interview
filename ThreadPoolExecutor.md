ThreadPoolExecutor.execute方法
提交任务到线程池
```java
    public void execute(Runnable command) {
        if (command == null)
            throw new NullPointerException();
        //拿到ctl的value值
        int c = ctl.get();
        //线程池中的线程数小于核心线程数
        if (workerCountOf(c) < corePoolSize) {
            //调用addWorker方法，
            //如果addWorker返回true说明新建了线程来运行command
            if (addWorker(command, true))
                //返回
                return;
            //需要再get一次
            c = ctl.get();
        }
        //当前线程池的状态在运行中且command可以加入到workQueue中
        if (isRunning(c) && workQueue.offer(command)) {
            //再次get一次
            int recheck = ctl.get();
            //1.当前线程池已经不在运行状态
            //2.把commad从workQueue中移除
            //3.如果移除成功，执行拒绝策略
            if (! isRunning(recheck) && remove(command))
                //执行拒绝策略
                //这里说明两个情况：
                //1.线程池已经关闭了 
                //2.任务从队列里面已经移除了
                reject(command);
            //执行到这里说明可能有两种情况  
            //1.线程池未关闭 
            //2.command未从队列移除
            //然后判断线程池中的线程数是否等于0
            else if (workerCountOf(recheck) == 0)
                //调用addWorker方法，说明会新增worker线程
                addWorker(null, false);
        }
        //执行到这里说明 1.可能线程池关闭了 2.队列满了
        //再次尝试执行addWorker新增线程执行任务，
        //如果addWorker返回false执行拒绝策略
        else if (!addWorker(command, false))
            //执行拒绝策略
            reject(command);
    }
```

ThreadPoolExecutor.addWorker方法
新增线程执行任务
```java
    private boolean addWorker(Runnable firstTask, boolean core) {
        //设置跳出点
        retry:
        for (;;) {
            //获取到value值
            int c = ctl.get();
            //拿到当前状态值
            int rs = runStateOf(c);

            // Check if queue empty only if necessary.
            // rs >= SHUTDOWN 说明线程池已经终止(处于SHUTDOWN,STOP,TIDYING,TERMINATED的一种)
            // (rs == SHUTDOWN && firstTask == null && !workQueue.isEmpty())
            // 为true的情况是状态是SHUTDOWN, firstTask== null, workQueue非空
            /**
             *  看一下什么情况会return false
             *  1.rs != SHUTDOWN, 那当前线程池的状态必然是(STOP,TIDYING,TERMINATED)的其中一个了
             *  这个时候说明线程池已经不能再接收任务了，也不能新增线程，所以return false
             *  2. rs == SHUTDOWN,然后第二个条件不满足也就是firstTask != null
             *  因为SHUTDOWN状态下线程池不能接收新的任务，所以return false
             *  3. rs == SHUTDOWN && firstTask == null然后第三个条件不满足，也就是
             *  workQueue是空的, 这种情况也是直接return false
             *
             */
            if (rs >= SHUTDOWN &&
                ! (rs == SHUTDOWN &&
                   firstTask == null &&
                   ! workQueue.isEmpty()))
                return false;

            /**
             *
             * 死循环
             */
            for (;;) {
                //拿到当前线程池中的有效线程数
                int wc = workerCountOf(c);
                /**
                 * 判断线程数
                 * 当以下两种情况返回false
                 * 1.当前线程数大于等于容量(536870912 - 1) 基本不可能达到
                 * 2.第二种情况又跟传入的参数有关如果
                 * core == true说明需要创建核心线程，如果当前线程数量 >= 核心线程数说明核心线程数已满，
                 * 无法创建， 所以返回false
                 * core == false说明需要创建非核心线程，判断如果当前线程数量 >= 线程数的最大值
                 * 说明也无法创建线程了，所以返回false
                 *
                 */
                if (wc >= CAPACITY ||
                    wc >= (core ? corePoolSize : maximumPoolSize))
                    return false;
                //走到这里说明前面的条件都满足了
                //然后线程的个数加1
                if (compareAndIncrementWorkerCount(c))
                    //加1成功了，跳出来循环
                    break retry;
                //没成功，说明可能有别的线程成功了，
                //然后再次拿值
                c = ctl.get();  // Re-read ctl
                //判断一下当前状态值跟之前进来循环之前是不是一样的
                //如果不一样，再次进入循环的开始.这边是可能有别的线程调用了，导致状态可能被改了(CAS+double check应用)
                if (runStateOf(c) != rs)
                    continue retry;
                // else CAS failed due to workerCount change; retry inner loop
            }
        }

        /**
         * 首先能走到这里，说明可以创建线程了
         * 定义两个标识
         */
        boolean workerStarted = false;
        boolean workerAdded = false;
        Worker w = null;
        try {
            //创建一个Worker
            w = new Worker(firstTask);
            //拿到Worker里面的线程
            final Thread t = w.thread;
            //判断空.感觉这里没有必要判断空，因为Worker是new出来
            if (t != null) {
                /**
                 * 拿到锁
                 */
                final ReentrantLock mainLock = this.mainLock;
                mainLock.lock();
                try {
                    // Recheck while holding lock.
                    // Back out on ThreadFactory failure or if
                    // shut down before lock acquired.
                    // 拿到锁之后再次检查
                    int rs = runStateOf(ctl.get());

                    /**
                     * 还是两种情况
                     * 1. rs < SHUTDOWN
                     * 2. rs == SHUTDOWN && fistTask == null
                     */
                    if (rs < SHUTDOWN ||
                        (rs == SHUTDOWN && firstTask == null)) {
                        //判断线程是否alive
                        if (t.isAlive()) // precheck that t is startable
                            throw new IllegalThreadStateException();
                        //把worker加到workers里面
                        workers.add(w);
                        int s = workers.size();
                        if (s > largestPoolSize)
                            largestPoolSize = s;
                        //把标识workerAdded置为true
                        workerAdded = true;
                    }
                } finally {
                    mainLock.unlock();
                }
                //如果worker已经加入到workers里面了
                if (workerAdded) {
                    //启动线程
                    t.start();
                    //设置标识workerStarted=true
                    workerStarted = true;
                }
            }
        } finally {
            if (! workerStarted)
                addWorkerFailed(w);
        }
        return workerStarted;
    }
```

ThreadPoolExecutor.runWorker方法
任务执行的开始
```java
final void runWorker(Worker w) {
        //拿到当前线程的引用
        Thread wt = Thread.currentThread();
        //拿到应该执行的任务
        Runnable task = w.firstTask;
        //把w.firstTask置为null
        w.firstTask = null;
        //这里不知道是干嘛
        w.unlock(); // allow interrupts
        //声明了一个变量completedAbruptly
        boolean completedAbruptly = true;
        try {
            /**
             * 这里也分两种情况
             * 第一种 task != null
             * 第二种 从阻塞队列里能拿到任务
             * 如果都不满足，说明该worker需要退出了say bye bye
             *
             */
            while (task != null || (task = getTask()) != null) {
                //上锁，不太清楚这里为啥要上锁
                w.lock();
                // If pool is stopping, ensure thread is interrupted;
                // if not, ensure thread is not interrupted.  This
                // requires a recheck in second case to deal with
                // shutdownNow race while clearing interrupt

                /**
                 * 这里有很多条件，如果满足了这些条件会调用wt.interrupt() 线程中断
                 * 因为现在的线程肯定是在运行中，因此执行wt.interrupt()也仅仅只会设置一个中断标识
                 * 下面是哪些条件能进入if语句块：
                 *
                 * (runStateAtLeast(ctl.get(), STOP) || (Thread.interrupted() && runStateAtLeast(ctl.get(), STOP)))
                 * 返回true
                 * wt.isInterrupted()返回false
                 *
                 * 1. (runStateAtLeast(ctl.get(), STOP) 说明线程池已经STOP了 rs >= STOP
                 * 2. (Thread.interrupted() && runStateAtLeast(ctl.get(), STOP)) rs >= STOP且当前线程已经中断了
                 *
                 * 总结一下能进入执行 wt.interrupt()的条件：
                 * 1.当前线程池状态 rs >= STOP且wt.isInterrupted()=false=当前线程未中断
                 * 2.rs < STOP(那rs可能处于 Running或者SHUTDOWN)
                 * 当前线程已经被中断了，然后再检查一下状态(需要处于STOP以上状态)并且没有被中断
                 * 执行中断
                 *
                 */
                if ((runStateAtLeast(ctl.get(), STOP) ||
                     (Thread.interrupted() &&
                      runStateAtLeast(ctl.get(), STOP))) &&
                    !wt.isInterrupted())
                    wt.interrupt();

                try {
                    //执行开始之前
                    beforeExecute(wt, task);
                    Throwable thrown = null;
                    try {
                        //执行任务
                        task.run();
                    } catch (RuntimeException x) {
                        thrown = x; throw x;
                    } catch (Error x) {
                        thrown = x; throw x;
                    } catch (Throwable x) {
                        thrown = x; throw new Error(x);
                    } finally {
                        //空方法，子类可以实现
                        afterExecute(task, thrown);
                    }
                } finally {
                    task = null;
                    //完成的任务数增加
                    w.completedTasks++;
                    //解锁
                    w.unlock();
                }
            }
            //设置 completedAbruptly = false
            completedAbruptly = false;
        } finally {
            //处理worker退出,回收worker
            //如果completedAbruptly=true说明可能哪里有异常
            processWorkerExit(w, completedAbruptly);
        }
    }
```

ThreadPoolExecutor.processWorkerExit
线程退出
```java
    private void processWorkerExit(Worker w, boolean completedAbruptly) {
        //// 如果Worker没有正常结束流程调用processWorkerExit方法，worker数量减一。如果是正常结束的话，在getTask方法里worker数量已经减一了
        if (completedAbruptly) // If abrupt, then workerCount wasn't adjusted
            decrementWorkerCount();

        //拿到锁
        final ReentrantLock mainLock = this.mainLock;
        //加锁
        mainLock.lock();
        try {
            //完成的总任务数递增
            completedTaskCount += w.completedTasks;
            //把worker从workers移除
            workers.remove(w);
        } finally {
            //释放锁
            mainLock.unlock();
        }

        // 尝试结束线程池
        tryTerminate();

        int c = ctl.get();
        if (runStateLessThan(c, STOP)) {
            if (!completedAbruptly) {
                int min = allowCoreThreadTimeOut ? 0 : corePoolSize;
                if (min == 0 && ! workQueue.isEmpty())
                    min = 1;
                if (workerCountOf(c) >= min)
                    return; // replacement not needed
            }
            addWorker(null, false);
        }
    }
```

ThreadPoolExecutor.tryTerminate
尝试终止线程池
```java
    final void tryTerminate() {
        for (;;) {
            int c = ctl.get();
            /**
             * 以下三种情况会直接return
             * 1.isRunning(c) 当前线程池状态为运行中
             * 2.runStateAtLeast(c, TIDYING) 当前线程池的状态 >= TIDYING
             * TIDYING状态： 说所有的任务都执行完了，当前线程池已经没有有效的线程
             * 3.(runStateOf(c) == SHUTDOWN && ! workQueue.isEmpty())
             * 线程池状态 == SHUTDOWN状态并且阻塞队列非空
             *
             */
            if (isRunning(c) ||
                runStateAtLeast(c, TIDYING) ||
                (runStateOf(c) == SHUTDOWN && ! workQueue.isEmpty()))
                return;
            /**
             *
             * 这里说明前面的条件不满足
             * 1.当前状态不为运行中
             * 2.rs < TIDYING (那状态可能为 STOP, SHUTDOWN)
             * 总结一下到这里的情况：
             * 1.线程池的状态为STOP(不接受新的任务，不处理阻塞队列里的任务，中断正在处理的任务)
             * 2.线程池当前的状态为SHUTDOWN且阻塞队列是空的(不接受新的任务，但是可以处理阻塞队列里的任务)
             */

            /**
             * 如果当前线程数不等于0
             */
            if (workerCountOf(c) != 0) { // Eligible to terminate
                /**
                 *
                 * 设置中断，
                 * ONLY_ONE表示只设置一个线程的中断标识，
                 * 因为被唤醒的那个线程同样会执行到这里，知道所有的线程都退出
                 */
                interruptIdleWorkers(ONLY_ONE);
                return;
            }

            /**
             *
             * 能执行到这里说明
             * 1.线程池状态为STOP或者SHUTDOWN
             * 2.所有的线程都被中断完毕，退出了
             */
            //拿到锁对象
            final ReentrantLock mainLock = this.mainLock;
            //加锁
            mainLock.lock();
            try {
                //CAS 修改当前状态为TIDYING
                if (ctl.compareAndSet(c, ctlOf(TIDYING, 0))) {
                    try {
                        //空方法，留给子类实现,表示线程池终止了
                        terminated();
                    } finally {
                        //修改线程池状态为TERMINATED
                        ctl.set(ctlOf(TERMINATED, 0));
                        //唤醒条件变量termination
                        termination.signalAll();
                    }
                    return;
                }
            } finally {
                mainLock.unlock();
            }
            // else retry on failed CAS
        }
    }
```