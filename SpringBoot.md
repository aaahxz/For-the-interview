[TOC]

### 1. SringBoot自动配置原理？

- SpringBoot启动的时候加载主配置类，开启了自动配置功能@EnableAutoConfiguration。
- @EnableAutoConfiguration的作用是利用AutoConfigurationImportSelector给容器中导入一些组件。
- 通过protected List<String> getCandidateConfigurations(AnnotationMetadata metadata,    AnnotationAttributes attributes)获取候选的配置，这个是扫描所有jar包类路径下"META-INF/spring.factories";
- 然后把扫描到的这些文件包装成Properties对象。
- 从properties中获取到EnableAutoConfiguration.class类名对应的值，然后把他们添加在容器中。
- 整个过程就是将类路径下"META-INF/spring.factories"里面配置的所有EnableAutoConfiguration的值加入到容器中。
- 每一个这样XXAutoConfiguration类都是容器中的一个组件都加入到容器中，用他们来做自动配置。每一个自动配置类进行自动配置功能，
- 根据当前不同的条件判断，决定这个配置是否生效。

### 2. SpringBoot starter 原理？

`spring-boot`启动的时候会找到`starter` `jar`包中的`resources/META-INF/spring.factories`文件，根据`spring.factories`文件中的配置，找到需要自动配置的类



### 3. SpringBoot 启动流程？

- 创建SpringApplication实例
- 扫描/META-INF/spring.factories 文件 
  - 将传入的启动类"com.example.ms.DemoApplication"放入Set集合中
  - 判断是否为Web环境：存在（javax.servlet.Servlet && org.springframework.web.context.ConfigurableWebApplicationContext ）类则是
  - 创建并初始化ApplicationInitializer列表 （spring.factories）
  - 创建并初始化ApplicationListener列表  （spring.factories）
  - 初始化主类mainApplicatioClass    (DemoApplication)
- 启动run方法
  - 创建计时器StopWatch
  - 配置awt系统属性
  - 获取SpringApplicationRunListeners
  - 启动SpringApplicationRunListeners
  - 创建ApplicationArguments
  - 创建并初始化ConfigurableEnvironment
  - 打印Banner
  - 创建ConfigurableApplicationContext
  - 准备ConfigurableApplicationContext
  - 刷新ConfigurableApplicationContext
  - 容器刷新后动作
  - SpringApplicationRunListeners发布finish事件
  - 计时器停止计时

### 4. Spring如何解决循环依赖？

#### 4.1 什么是循环依赖？

举个例子：

```java
class A {
    public A() {
        new B();
    }
}

class B {
    public B() {
        new A();
    }
}
```

上面就是一个典型的循环依赖的例子，只不过